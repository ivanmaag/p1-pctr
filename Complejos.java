
/** 
 * Práctica 1 - PCTR
 * 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

public class Complejos {
    //Atributos privados
    private double [] complejo;

    public Complejos() {} //Constructor vacío

    /**
     * Constructor. Crea un número complejo a partir de la parte real y la parte imaginaria.
     * @param real
     * @param imag
     */
    public Complejos(double real,
                     double imag) {

        complejo = new double[2];
        complejo[0] = real;
        complejo[1] = imag;
    }
    
    public double getReal() { return complejo[0]; } //Observador del parámetro parte real
    public void setReal(double real) { complejo[0] = real; } //Modificador del parámetro parte real

    public double getImag() { return complejo[1]; } //Observador del parámetro parte imaginaria
    public void setImag(double imag) { complejo[1] = imag; } //Modificador del parámetro parte imaginaria

    public String toString() {
        return "z=" + complejo[0] + "+" + complejo[1] + "i";
    }

    /**
     * Método que suma dos números complejos.
     * @param c object Complejo
     * @return object Complejo
     */
    public Complejos suma(Complejos c) {
		double x = this.getReal() + c.getReal();
		double y = this.getImag() + c.getImag();

		return new Complejos(x, y);
    }

    /**
     * Método que resta dos números complejos.
     * @param c object Complejo
     * @return object Complejo
     */
    public Complejos resta(Complejos c) {
		double x = this.getReal() - c.getReal();
		double y = this.getImag() - c.getImag();

		return new Complejos(x, y);
    }

    /**
     * Método que multiplica dos números complejos.
     * @param c object Complejo
     * @return object Complejo
     */
    public Complejos producto(Complejos c) {
		double x = ((this.getReal() * c.getReal()) + (this.getImag() * c.getImag()));
		double y = ((this.getImag() * c.getReal()) - (this.getReal() * c.getImag()));

		return new Complejos(x, y);
    }
    
    /**
     * Método que divide dos números complejos.
     * @param c object Complejo
     * @return object Complejo
     */
    public Complejos cociente(Complejos c) {
        double x = ((this.getReal() * c.getReal()) + (this.getImag() * c.getImag())) /
                    (Math.pow(c.getReal(), 2) + Math.pow(c.getImag(), 2));
        double y = ((this.getImag() * c.getReal()) - (this.getReal() * c.getImag())) /
                    (Math.pow(c.getReal(),2) + Math.pow(c.getImag(), 2));

		return new Complejos(x, y);
    }
    
    /**
     * Método que calcula el módulo de un número complejo.
     * @param c object Complejo
     * @return object Complejo
     */
    public double modulo() {
		return (Math.sqrt(Math.pow(complejo[0], 2) + Math.pow(complejo[1], 2)));
	}
}
