/** 
 * Práctica 1 - PCTR
 * 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

public class CuerpoAstrofisico {
    //Atributos privados
    protected String name;
    protected double diameter;
    protected double mass;
    protected double volume;
    protected double density;

    public CuerpoAstrofisico() {} //Constructor vacío

    /**
    * @param name String
    * @param diameter double 
    * @param mass double 
    * @param volume double 
    * @param density double 
	* @exception No No se lanzan excepciones
	*/
    public CuerpoAstrofisico(String name,
                             double diameter,
                             double mass,
                             double volume,
                             double density) {

        this.name = name;
        this.diameter = diameter;
        this.mass = mass;
        this.volume = volume;
        this.density = density;
    }

    public String getName() { return name; } //Observador del parámetro nombre
    public void setName(String name) { this.name = name; } //Modificador del parámetro nombre

    public double getDiameter() { return diameter; } //Observador del parámetro diámetro
    public void setDiameter(double diameter) { this.diameter = diameter; } //Modificador del parámetro diámetro

    public double getMass() { return mass; } //Observador del parámetro masa
    public void setMass(double mass) { this.mass = mass; } //Modificador del parámetro masa

    public double getVolume() { return volume; } //Observador del parámetro volumen
    public void setVolume(double volume) { this.volume = volume; } //Modificador del parámetro volumen

    public double getDensity() { return density; } //Observador del parámetro densidad
    public void setDensity(double density) { this.density = density; } //Modificador del parámetro densidad

    public String toString() {
        return name + ":" + diameter + ":" + mass + ":" + volume + ":" + density;
    }
}
