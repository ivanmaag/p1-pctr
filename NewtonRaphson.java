
/**
 * Práctica 1 - PCTR 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

public class NewtonRaphson {

    /**
    * @param x double
	* @exception No No se lanzan excepciones.
    */
    public static double f(double x) {
        double res=0;
        res = Math.cos(x)-Math.pow(x,3);
        return res;
    }

    /**
    * @param x double
	* @exception No No se lanzan excepciones.
    */
    public static double fprima(double x) {
        double res=0;
        res = - Math.sin(x)-3*Math.pow(x,2);
        return res;
    }

    /**
    * @param x double
	* @exception No No se lanzan excepciones.
    */
    public static double g(double x) {
        double res=0;
        res = Math.pow(x,2) - 5;
        return res;
    }

    /**
    * @param x double
	* @exception No No se lanzan excepciones.
    */
    public static double gprima(double x) {
        double res=0;
        res = 2*x;
        return res;
    }

    /**
    * @param args String[]. Parámetro [0] aproximación inicial (int). Parámetro [1] número de iteraciones (int)
	* @exception Sí Se lanzan excepciones. Necesario introducir dos números enteros por línea de comandos.
    */
    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.out.println("Por favor, introduzca la aproximación inicial y el número de iteraciones por consola");
            System.exit(-1);
        }
        int x0 = Integer.parseInt(args[0]); //La aproximación inicial x_0 se introduce por terminal
        int nIter = Integer.parseInt(args[1]); //El número de iteraciones se introduce por terminal

        BufferedReader dato = new BufferedReader(new InputStreamReader(System.in));
        int opt;
        double xN, xN1;

        do {
            System.out.print("++++ NEWTON-RAPHSON ++++\n" +
                             " 1. f(x) = cos(x)-x³ en [0, 1]\n" +
                             " 2. f(x) = x²-5 en [2, 3]\n" +
                             " 0. Salir\n" +
                             "-> Elige opción: ");
            
            opt = Integer.parseInt(dato.readLine());
            
            switch(opt) {
                case 1: System.out.println("Encontrando el cero de la función...");
                        xN = x0;
                        for (int i=0; i<=nIter; ++i) {
                            if (fprima(xN)!=0) {
                                xN1 = xN - f(xN) / fprima(xN);
                                System.out.println("Iteración: " + i + " | Aproximación: " + xN1);
                                xN = xN1;
                            }
                        }
                        System.out.println("\nResultado: " + xN);
                        System.out.print("\n");
                        TimeUnit.SECONDS.sleep(2);
                        break;
                case 2: System.out.println("Encontrando el cero de la función...");
                        xN = x0;
                        for (int i=0; i<=nIter; ++i) {
                            if (gprima(xN)!=0) {
                                xN1 = xN - g(xN) / gprima(xN);
                                System.out.println("Iteración: " + i + " | Aproximación: " + xN1);
                                xN = xN1;
                            }
                        }
                        System.out.println("\nResultado: " + xN);
                        System.out.print("\n");
                        TimeUnit.SECONDS.sleep(2);
                        break;
                case 0: System.out.println("Saliendo...");
                        TimeUnit.SECONDS.sleep(1);
                        break;
                default: System.out.println("Error: Introduzca una opción correcta.");
                        TimeUnit.SECONDS.sleep(1);
                        System.out.print("\n");
            }
        } while (opt!=0);
    }
}
