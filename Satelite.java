/** 
 * Práctica 1 - PCTR
 * 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

public class Satelite extends CuerpoAstrofisico {
    //Atributos privados
    private double orbitalSpeed;

    public Satelite() {} //Constructor vacío

    /**
    * @param name String
    * @param diameter double 
    * @param mass double 
    * @param volume double 
    * @param density double 
	* @param orbitalSpeed double
	* @exception No No se lanzan excepciones
	*/
    public Satelite(String name,
                    double diameter,
                    double mass,
                    double volume,
                    double density,
                    double orbitalSpeed) {
                        
        super(name, diameter, mass, volume, density);
        this.orbitalSpeed = orbitalSpeed;
    }

    public double getOrbitalSpeed() { return orbitalSpeed; } //Observador del parámetro velocidad orbital
    public void setOrbitalSpeed(double orbitalSpeed) { this.orbitalSpeed = orbitalSpeed; } //Modificador del parámetro velocidad orbital

    public String toString() {
        return name + ":" + diameter + ":" + mass + ":" + volume + ":" + density + ":" + orbitalSpeed;
    }
}
