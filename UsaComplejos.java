
/** 
 * Práctica 1 - PCTR
 * 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

import java.util.Scanner;

public class UsaComplejos {

    /** Método main que se lanza al ejecutar el programa. 
     * @param args Array de cadenas de texto que almacena los argumentos de consola. Recibe tantas
     * cadenas como el usuario instroduzca, que parsea como double.
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Complejo A:");
		System.out.print(" Parte real: ");
		double a1 = sc.nextDouble();
		System.out.print(" Parte imaginaria: ");
		double a2 = sc.nextDouble();
		Complejos complejoA = new Complejos(a1, a2);

		System.out.println("Complejo B:");
		System.out.print(" Parte real: ");
		double b1 = sc.nextDouble();
		System.out.print(" Parte imaginaria: ");
		double b2 = sc.nextDouble();
		Complejos complejoB = new Complejos(b1, b2);
        Complejos complejoC = new Complejos();
        
        int op;

        do {
            System.out.println();
            System.out.println("a+bi Complejos C≈RxR");

            System.out.println("1. Suma");
            System.out.println("2. Resta");
            System.out.println("3. Módulo");
            System.out.println("4. Producto");
            System.out.println("5. Cociente");
            System.out.println("0. Salir");
            System.out.print("Opción: ");
            op = sc.nextInt();

            switch (op) {
            case 1:
                complejoC = complejoA.suma(complejoB);
                System.out.println();
                System.out.println("Parte real: " + complejoC.getReal());
                System.out.println("Parte imaginaria: " + complejoC.getImag());
                break;

            case 2:
                complejoC = complejoA.resta(complejoB);
                System.out.println();
                System.out.println("Parte real: " + complejoC.getReal());
                System.out.println("Parte imaginaria: " + complejoC.getImag());
                break;

            case 3:
                System.out.println();
                System.out.println("Complejo A: " + complejoA.modulo());
                System.out.println("Complejo B: " + complejoB.modulo());
                break;

            case 4:
                complejoC = complejoA.producto(complejoB);
                System.out.println();
                System.out.println("Parte real: " + complejoC.getReal());
                System.out.println("Parte imaginaria: " + complejoC.getImag());
                break;

            case 5:
                System.out.println();
                complejoC = complejoA.cociente(complejoB);
                System.out.println("Parte real: " + complejoC.getReal());
                System.out.println("Parte imaginaria: " + complejoC.getImag());
                break;

            case 0:
                System.out.println("Saliendo -->");
                break;

            default:
                System.err.println("Error: Opción no válida.");
                break;
            }
        } while (op != 0);
        sc.close();
    }
}
