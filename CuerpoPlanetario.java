/** 
 * Práctica 1 - PCTR
 * 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

public class CuerpoPlanetario extends CuerpoAstrofisico {
    //Atributos privados
    private double surfaceGravity;

    public CuerpoPlanetario() {} //Constructor vacío

    /**
    * @param name String
    * @param diameter double 
    * @param mass double 
    * @param volume double 
    * @param density double 
	* @param surfaceGravity double
	* @exception No No se lanzan excepciones
	*/
    public CuerpoPlanetario(String name,
                            double diameter,
                            double mass,
                            double volume,
                            double density,
                            double surfaceGravity) {

        super(name, diameter, mass, volume, density);
        this.surfaceGravity = surfaceGravity;
    }

    public double getSurfaceGravity() { return surfaceGravity; } //Observador del parámetro gravedad superficial
    public void setSurfaceGravity(double surfaceGravity) { this.surfaceGravity = surfaceGravity; } //Modificador del parámetro gravedad superficial

    public String toString() {
        return name + ":" + diameter + ":" + mass + ":" + volume + ":" + density + ":" + surfaceGravity;
    }
}
