/** 
 * Práctica 1 - PCTR
 * 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

public class Estrella extends CuerpoAstrofisico {
    //Atributos privados
    private double luminosity;

    public Estrella() {} //Constructor vacío

    /**
    * @param name String
    * @param diameter double 
    * @param mass double 
    * @param volume double 
    * @param density double 
	* @param luminosity double
	* @exception No No se lanzan excepciones
	*/
    public Estrella(String name,
                    double diameter,
                    double mass,
                    double volume,
                    double density,
                    double luminosity) {
        
        super(name, diameter, mass, volume, density);
        this.luminosity = luminosity;
    }

    public double getLuminosity() { return luminosity; } //Observador del parámetro luminosidad
    public void setLuminosity(double luminosity) { this.luminosity = luminosity; } //Modificador del parámetro luminosidad

    public String toString() {
        return name + ":" + diameter + ":" + mass + ":" + volume + ":" + density + ":" + luminosity;
    }
}
