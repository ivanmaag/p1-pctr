
/**
 * Práctica 1 - PCTR 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */


public class intDefinidaMonteCarlo {
    /**
    * @param args String[]. Parámetro [0] número de iteraciones (int).
	* @exception Sí Se lanzan excepciones. Necesario introducir un número entero por línea de comandos.
	*/
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Por favor, introduzca el número de iteraciones por consola");
            System.exit(-1);
        }
        int n = Integer.parseInt(args[0]); //El número de iteraciones se introduce por terminal
        double aciertos=0;
        double ratio=0;

        for (int i=0; i<=n; ++i) {
            double coordenada_x=Math.random();
            double coordenada_y=Math.random();

            if (coordenada_y <= coordenada_x) {
                aciertos++;
            }
        }

        ratio=aciertos/n;
        System.out.println("Integral aproximada: " + ratio);
    }
}
