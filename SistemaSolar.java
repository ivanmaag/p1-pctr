/** 
 * Práctica 1 - PCTR
 * 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

public class SistemaSolar extends Estrella {

    /**
    * @param args String[]
	* @exception No No se lanzan excepciones
	*/
    public static void main(String[] args) {
        //Creamos el objeto sol de la clase Estrella 
        Estrella sol = new Estrella("sol", 1.391, 1.9891, 1.4123, 1411.0, 3.827);
        System.out.println(sol.toString());

        //Creamos el objeto tierra de la clase CuerpoPlanetario
        CuerpoPlanetario tierra = new CuerpoPlanetario("tierra", 12742.0, 5.9736, 1.08321, 5.515, 5.10072);
        System.out.println(tierra.toString());

        //Creamos el objeto luna de la clase Satélite
        Satelite luna = new Satelite("luna", 3474.2, 7.349, 2.1958, 3.34, 1.022);
        System.out.println(luna.toString());
    }
}
