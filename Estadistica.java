
/**
 * Práctica 1 - PCTR 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

public class Estadistica {
    
    /**
    * @param muestra double[]
	* @exception No No se lanzan excepciones
	*/
    public static double media(double[] muestra) {
        double media=0;
        for (int i=0; i<muestra.length; ++i) {
            media += muestra[i];
        }
        media /= muestra.length;
        return media;
    }

    /**
    * @param muestra double[]
	* @exception No No se lanzan excepciones
	*/
    public static double moda(double[] muestra) {
        double moda=0;
        int max=0;
        for (int i=0; i<muestra.length; ++i) {
            int repes=0;
            for (int j=0; j<muestra.length; ++j) {
                if (muestra[i]==muestra[j])
                    repes++;
                if (repes>max) {
                    moda=muestra[i];
                    max=repes;
                }
            }
        }
        return moda;
    }

    /**
    * @param muestra double[]
	* @exception No No se lanzan excepciones
	*/
    public static double varianza(double[] muestra) {
        double varianza=0;
        double media=media(muestra);
        for (int i=0; i<muestra.length; ++i) {
            varianza = Math.pow(muestra[i] - media, 2);
        }
        varianza /= muestra.length-1;
        return varianza;
    }

    /**
    * @param muestra double[]
	* @exception No No se lanzan excepciones
	*/    
    public static double desviacionTipica(double[] muestra) {
        double desviacionTipica=0;
        desviacionTipica = Math.sqrt(varianza(muestra));
        return desviacionTipica;
    }

    /**
    * @param args String[]
	* @exception Sí Se lanzan excepciones. Necesario introducir un número entero por línea de comandos.
	*/
    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            System.out.println("Por favor, introduzca un entero por consola");
            System.exit(0);
        }

        int param = Integer.parseInt(args[0]); //La longitud de muestra numéricos se introduce por terminal
        double [] muestra = new double[param];

        //Introducimos los muestra numéricos
        BufferedReader dato = new BufferedReader(new InputStreamReader(System.in));
        for (int i=0; i<muestra.length; ++i) {
            System.out.print("Introduce valor numérico: ");
            muestra[i] = Double.parseDouble(dato.readLine());
        }

        //Mostramos por pantalla los muestra numéricos introducidos para su comprobación
        for (int i=0; i<muestra.length; ++i) {
            System.out.print(muestra[i] + ", ");
        }
        System.out.print("\n\n");

        //Mostramos el menú por pantalla
        int opt;
        do {
            System.out.print("++++ ESTADISTICA ++++\n" +
                             " 1. Media\n" +
                             " 2. Moda\n" +
                             " 3. Varianza\n" +
                             " 4. Desviación típica\n" +
                             " 0. Salir\n" +
                             "-> Elige opción: ");
            
            opt = Integer.parseInt(dato.readLine());
            
            switch(opt) {
                case 1: System.out.println("Calculando media...");
                        TimeUnit.SECONDS.sleep(1);
                        System.out.println("Media: " + media(muestra));
                        System.out.print("\n");
                        TimeUnit.SECONDS.sleep(2);
                        break;
                case 2: System.out.println("Calculando moda...");
                        TimeUnit.SECONDS.sleep(1);
                        System.out.println("Moda: " + moda(muestra));
                        System.out.print("\n");
                        TimeUnit.SECONDS.sleep(2);
                        break;
                case 3: System.out.println("Calculando varianza...");
                        TimeUnit.SECONDS.sleep(1);
                        System.out.println("Varianza: " + varianza(muestra));
                        System.out.print("\n");
                        TimeUnit.SECONDS.sleep(2);
                        break;
                case 4: System.out.println("Calculando desviación típica...");
                        TimeUnit.SECONDS.sleep(1);
                        System.out.println("Desviación típica: " + desviacionTipica(muestra));
                        System.out.print("\n");
                        TimeUnit.SECONDS.sleep(2);
                        break;
                case 0: System.out.println("Saliendo...");
                        TimeUnit.SECONDS.sleep(1);
                        break;
                default: System.out.println("Error: Introduzca una opción correcta.");
                        TimeUnit.SECONDS.sleep(1);
                        System.out.print("\n");
            }
        } while (opt!=0);
    }
}
